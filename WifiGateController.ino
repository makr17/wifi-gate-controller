// REST server to control two relays tied to an Eagle-100 gate controller

#include <Arduino.h>

#include <stdint.h>
#include <stdbool.h>
//#include <DataStructures.h>
#include <Utilities.h>

#include <ArduinoJson.h>
#include <WiFi.h>
#include <WebServer.h>
#include <HTTPUpdateServer.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#include "secrets.h"

WebServer httpRestServer(HTTP_PORT);
HTTPUpdateServer httpUpdater;

// GPIO 25: gate exit (open)
#define OPEN_PIN 25
// GPIO 27: gate close
#define HOLD_PIN 27

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_SERVER);

void setup() {
  // setup pinmodes for output
  pinMode(OPEN_PIN,    OUTPUT);
  pinMode(HOLD_PIN,   OUTPUT);
  // turn off built-in LED
  digitalWrite(BUILTIN_LED, HIGH);
  // set output pins high
  digitalWrite(OPEN_PIN, HIGH);
  digitalWrite(HOLD_PIN, HIGH);
  
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.hostname(WIFI_HOSTNAME);
  delay(100);
  WiFi.begin(SECRET_WIFI_SSID, SECRET_WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" WIFI CONNECTED");
  
  timeClient.begin();
  timeClient.setTimeOffset(0); // UTC
  String formattedTime = timeClient.getFormattedTime();
  Serial.print("Current Time: ");
  Serial.println(formattedTime);
  
  restServerRouting();
  httpUpdater.setup(&httpRestServer);
  httpRestServer.begin();
}

void loop() {
  // make sure wifi is up
  // _should_ reconnect on its own _if_ the wifi code runs
  if (WiFi.status() != WL_CONNECTED) {
    delay(250);
    return;
  }
  // update time
  timeClient.update();
  // check for requests
  httpRestServer.handleClient();
  // avoid a tight loop
  delay(250);
}

void restServerRouting() {
    httpRestServer.on(F("/"),        HTTP_GET, getSlash);
    httpRestServer.on(F("/hold"),    HTTP_GET, getHold);
    httpRestServer.on(F("/release"), HTTP_GET, getRelease);
    httpRestServer.on(F("/open"),    HTTP_GET, getOpen);
}

void getSlash() {
  String body = "<html><body>";
  body += "<h1>WiFi Gate Controller</h1>";
  body += "current time is <b>" + formatCurrentDateTime() + "</b>";
  body += "</body></html>";
  httpRestServer.send(200, F("text/html"), body);
}

void getHold() {
  digitalWrite(HOLD_PIN, LOW);

  DynamicJsonDocument doc(2048);
  doc["action"] = "holding gate open";
  doc["timestamp"] = formatCurrentDateTime();
  String res;
  serializeJson(doc, res);
  httpRestServer.send(200, F("text/json"), res);
}

void getRelease() {
  digitalWrite(HOLD_PIN, HIGH);

  DynamicJsonDocument doc(2048);
  doc["action"] = "releasing gate";
  doc["timestamp"] = formatCurrentDateTime();
  String res;
  serializeJson(doc, res);
  httpRestServer.send(200, F("text/json"), res);
}

void getOpen() {
  digitalWrite(OPEN_PIN, LOW);
  
  DynamicJsonDocument doc(2048);
  doc["action"] = "opening gate";
  doc["timestamp"] = formatCurrentDateTime();
  String res;
  serializeJson(doc, res);
  httpRestServer.send(200, F("text/json"), res);
  
  delay(1500);
  
  digitalWrite(OPEN_PIN, HIGH);
}

String formatCurrentDateTime() {
  String dt;
  unsigned long epochTime = timeClient.getEpochTime();
  char buf[26];
  strftime(buf, 26, "%Y-%d-%mT%H:%M:%S+00:00", gmtime((time_t *)&epochTime));
  dt = String(buf);
  return dt;
}
