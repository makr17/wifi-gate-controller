# Wifi Gate Controller

Using an ESP32 Development Board and a Relay to make a WiFi-enabled opener
for an Eagle-100 gate motor.

The interface is pretty simple, just connect to

  `http://gate-controller/open`
  
to open the gate,

  `http://gate-controller/hold`

to hold it open, and

  `http://gate-controller/release`

to release the hold.  Our gate is set to autoclose, so no need for an
endpoint to close the gate.  I have the `open` endpoint wired to a
[button](https://www.home-assistant.io/integrations/button/)
entity in
[HomeAssistant](https://www.home-assistant.io/),
and `hold`/`release` wired to the state of an
[input boolean](https://www.home-assistant.io/integrations/input_boolean/).

The ESP32 is wired up to a double relay board.  The `open` pin is
wired to a relay which is in turn wired to the `Exit Loop` circuit on
the motor control board.  The `hold` pin is wired to the second relay
which is wired to the `Shadow Loop` circuit on the motor controller.

![Fritzing Diagram for the Controller](fritzing/WifiGateController.png)

The webserver is also configured for OTA updates.
Export a compiled binary from the Arduino IDE and upload via

  `http://gate-controller/update`

## Bill of Materials

* [An ESP32 development board](https://www.amazon.com/gp/product/B08246MCL5/)
* [A double 5V relay board](https://www.amazon.com/gp/product/B088GZ8ZPN/)
* [A proto board](https://www.amazon.com/gp/product/B00SK8QR8S/)
* [A box to hold it all](https://www.amazon.com/gp/product/B07NSV7CSM/)
* Wire
* Sugru to hold everything in place

## Dependencies

* [ArduinoJson](https://reference.arduino.cc/reference/en/libraries/arduinojson/)
* [NTPClient](https://reference.arduino.cc/reference/en/libraries/ntpclient/)
* [Utilities](https://www.arduino.cc/reference/en/libraries/utilities/)
* [Wifi](https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/api/wifi.html)

# Useful branches

The original version was for an
[ESP8266](https://gitlab.com/makr17/wifi-gate-controller/-/tree/esp8266)
and still exists on that branch.

I'm working on a
[rust](https://www.rust-lang.org/)
version on this
[branch](https://gitlab.com/makr17/wifi-gate-controller/-/tree/rebuild-in-rust)
but it isn't as stable as I'd like yet, and I don't have OTA updates working.
